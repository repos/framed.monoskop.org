#!/bin/sh
#

# Defaults
TMP=tmp



extract_fps()
{
	eval $(./bin/mpv_identify.sh ${SOURCE} | grep container_fps)
}

tests()
{
	# GNU sed needed for a line preprend trick
	if [ "$(sed --version | grep GNU)" ]
	then
		SED="sed"
		echo "[.] sed is gsed."
	else
		if [ "$(which gsed)" ]
		then
			echo "[.] Found gsed."
			SED="gsed"
		else
			echo "[!] Please install gsed."
		fi
	fi

	# testing options
    if [ ! -e ${SOURCE} ] || [ ! ${SOURCE} ]
    then
        echo "[!] Source unknown (${SOURCE})."
        echo "[!] Usage: ./bin/nfrm.sh -s source -p position -f frames"
        exit 1
    fi

    if [ ! ${POSITION} ]
    then
        echo "[!] Position not specified."
        POSITION=1
        echo "[.] Using position ${POSITION}"
    fi

    if [ ! ${FRAMES} ]
    then
        echo "[!] Frames not specified."
        FRAMES=3
        echo "[.] Using ${FRAMES} frames."
    fi

    if [ ! "${HEIGHT}" ]
    then
        echo "[!] Height not specified."
        echo "[.] Not resizing."
    fi
}

framed()
{


	#ffmpeg -ss ${POSITION} -i ${SOURCE} -r ${fps} -frames:v ${FRAMES} ${TMP}/${SOURCE}-${POSITION}-%03d.png

	SAFEPOSITION="blabla"
	ffmpeg -ss ${POSITION} -i ${SOURCE} -frames:v ${FRAMES} ${TMP}/${SOURCE}-${SAFEPOSITION}-%03d.png

	#OUTPUT="$(tr -d "\"\`'" <<<$OUTPUT)"
	mogrify -verbose -geometry x480 ${TMP}/${SOURCE}-${SAFEPOSITION}-*.png

    #FWD=$(/bin/ls ${TMP}/${SOURCE}-${SAFEPOSITION}-*.png | sed "s/${TMP}\///g")
    FWD=$(/bin/ls ${TMP}/${SOURCE}-${SAFEPOSITION}-*.png)
    RWD=$(echo "${FWD}" | ${SED} 1d | sort -r | ${SED} 1d)

	echo $FWD $RWD | tr " " "\n" > ${TMP}/${SOURCE}-${SAFEPOSITION}.txt
	${SED} -i "s/^.*$/file '&'/g" ${TMP}/${SOURCE}-${SAFEPOSITION}.txt
	${SED} -i "s/${TMP}\///g" ${TMP}/${SOURCE}-${SAFEPOSITION}.txt

	# WEBM generator

	ffmpeg -r ${container_fps} -f concat -i ${TMP}/${SOURCE}-${SAFEPOSITION}.txt -c:v libvpx-vp9 -pass 1 -b:v 1000K -threads 8 -speed 4 -tile-columns 6 -frame-parallel 1 -an -f webm /dev/null -y
	ffmpeg -r ${container_fps} -f concat -i ${TMP}/${SOURCE}-${SAFEPOSITION}.txt -c:v libvpx-vp9 -pass 2 -b:v 1000K -threads 8 -speed 1 -tile-columns 6 -frame-parallel 1 -an -f webm file:${SOURCE}-${SAFEPOSITION}.webm
    
	# GIF generator
	
	# old method
	#DELAY=$(echo "scale=10 ; (1.0 / ${container_fps}) * 100" | bc | sed 's/[.].*//')
    #gifsicle -O3 --delay ${DELAY} --loopcount=0 --colors 256 ${FWD} ${RWD} > ${SAFESOURCE}-${POSITION}-${FRAMES}.gif

	palette="${TMP}/palette.png"

	ffmpeg -r ${container_fps} -f concat -i ${TMP}/${SOURCE}-${SAFEPOSITION}.txt -vf "fps=${container_fps},palettegen" -y $palette
	ffmpeg -r ${container_fps} -f concat -i ${TMP}/${SOURCE}-${SAFEPOSITION}.txt -i $palette -lavfi "fps=${container_fps} [x]; [x][1:v] paletteuse" -y test.gif


    rm ${TMP}/${SOURCE}-${SAFEPOSITION}-*
	rm ffmpeg2pass-0.log
	
	# Rename file (OSX craps out with ':')
	#mv ${SOURCE}-${SAFEPOSITION}.webm  $(echo "${SOURCE}-${SAFEPOSITION}.webm" | tr ':' '.')
}

while getopts "s:p:f:h:" OPTIONS
do
    case ${OPTIONS} in
        s) SOURCE=${OPTARG};;
        p) POSITION=${OPTARG};;
        f) FRAMES=${OPTARG};;
        h) HEIGHT="-resize ${OPTARG}";;
        *) echo "hu?";;
   esac
done

tests
extract_fps
framed
